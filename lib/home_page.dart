import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:ball/ball_container.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});
  static void resetCounter(BuildContext context) {
    HomePageState state = context.findAncestorStateOfType<HomePageState>()!;
    state.resetCounter();
  }

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  Future<void> resetCounter() async {
    setState(() {
      counter = 0;
    });
  }

  late double maxH;
  bool up = false;
  double sizeBall = 30;
  double minH = 0;
  int counter = 0;

  @override
  Widget build(BuildContext context) {
    maxH = MediaQuery.of(context).size.height - sizeBall;
    return Stack(
      children: [
        GestureDetector(
          onTap: tapToScreen,
          child: Scaffold(
            body: Container(
              alignment: Alignment.topCenter,
              child: Ball(up: up ? maxH : minH),
            ),
          ),
        ),
        Align(
          alignment: Alignment.centerRight,
          child: Text(counter.toString()),
        )
      ],
    );
  }

  tapToScreen() {
    AudioPlayer().play(AssetSource('sounds/click.mp3'));
    setState(() {
      up = !up;
      counter++;
    });
  }
}
