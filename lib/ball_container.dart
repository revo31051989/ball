import 'package:flutter/material.dart';
import 'package:ball/home_page.dart';

class Ball extends StatelessWidget {
  final double up;
  get sizeBall => HomePageState().sizeBall;
  const Ball({super.key, required this.up});

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(seconds: 5),
      transform: Transform.translate(
        offset: Offset(0, up),
      ).transform,
      child: Container(
        width: sizeBall,
        height: sizeBall,
        decoration:
            const BoxDecoration(color: Colors.red, shape: BoxShape.circle),
      ),
      onEnd: () {
        HomePage.resetCounter(context);
      },
    );
  }
}
